class Contact < ApplicationRecord

    # Associations
    belongs_to :kind # reformatada na aula 18

    has_many :phones

    # Adicionado na aula 25

    has_one :address

    # Adicionado na aula 23

    accepts_nested_attributes_for :phones, allow_destroy: true
    # update_only adicionado na aula 27
    accepts_nested_attributes_for :address, update_only: true

    def as_json(options={})
        h = super(options)
        h[:birthdate] = (I18n.l(self.birthdate) unless self.birthdate.blank?)
        h
    end

    # def birthdate_br
    #     I18n.localize(self.birthdate) unless self.birthdate.blank?
    # end

    # comentado na aula 22

    # def to_br
    #     { name: self.name,
    #       email: self.email,
    #       birthdate: (I18n.l(self.birthdate) unless self.birthdate.blank?) }
    # end

    # def i18n
    #     I18n.default_locale
    # end

    # def hello
    #     I18n.t('hello')
    # end

    # optional: true adicionado na aula 18

    # belongs_to :kind, optional: true

    # comentado na aula 18

    # def author
    #     "Filipe Leite"
    # end

    # def kind_description
    #     self.kind.description
    # end

    # def as_json(options={})
    #     super(
    #         root:true,
    #         methods: [:kind_description],
    #         include: { kind: { only: :description}}
    #     )
    # end
end
