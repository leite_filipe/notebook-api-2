class ContactSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :birthdate

  # modificado na aula 42
  belongs_to :kind do
    link(:related) { contact_kind_url(object.id) }
  end

  has_many :phones do
    link(:related) { contact_phones_url(object.id) }
  end

  has_one :address do
    link(:related) { contact_address_url(object.id) }
  end

  # adicionado e modificado na aula 35

  # link(:self) { contact_path(object.id) }
  # link(:kind) { kind_path(object.id)}

  # comentado na aula 36

  # link(:self) { contact_url(object.id) }
  # link(:kind) { kind_url(object.kind.id) }


  # ------

  def attributes(*args)
    h = super(*args)
    h[:birthdate] = object.birthdate.to_time.iso8601 unless object.birthdate.blank?
    h
  end

end
