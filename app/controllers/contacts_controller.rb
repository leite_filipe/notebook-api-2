class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :update, :destroy]

  # GET /contacts
  def index
    @contacts = Contact.all

    render json: @contacts #, methods: [:hello, :i18n]

    # modificado na aula 32

    # render json: @contacts, methods: :birthdate_br [:hello, :i18n]
  end

  # GET /contacts/1
  def show
    # , include: [:kind, :phones, :address] comentado na aula 32/
    # , :address, :phones #adcionado na aula 28 na correção do serializer 
    render json: @contact, include: [:kind, :address, :phones] #, include: [:kind, :phones, :address]
  end

  # POST /contacts
  def create
    @contact = Contact.new(contact_params)

    if @contact.save
      render json: @contact, include: [:kind, :phones, :address], status: :created, location: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /contacts/1
  def update
    if @contact.update(contact_params)
      render json: @contact, include: [:kind, :phones, :address]
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /contacts/1
  def destroy
    @contact.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def contact_params
      # comentado na aula 39
      
      # params.require(:contact).permit(
      #   :name, :email, :birthdate, :kind_id,
      #   phones_attributes: [:id, :number, :_destroy],
      #   address_attributes: [:id, :street, :city])

      ActiveModelSerializers::Deserialization.jsonapi_parse(params)
    end
end
